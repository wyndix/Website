import * as React from 'react';
import "./App.css"

import { motion } from "framer-motion"
import { NextUIProvider, createTheme, Card, Grid, Text, Image, User, Navbar, Badge, Collapse, Spacer, Avatar, Link } from '@nextui-org/react';

const darkTheme = createTheme({
  type: 'dark',
});

const App = () => {
  // 2. Use at the root of your app
  return (
    <NextUIProvider theme={darkTheme}>
      <Navbar isBordered>
        <Navbar.Brand>
          <img src="/assets/logos/logo.svg" alt="Logo of Wyndix" /> <Spacer />
          <Badge color="error" content="ALPHA" enableShadow disableOutline>
            <Text b color="inherit" hideIn="xs">
              Wyndix's Website
            </Text>
          </Badge>
        </Navbar.Brand>
        <Navbar.Content hideIn="xs">
          <Navbar.Link isActive href="#">Home</Navbar.Link>
        </Navbar.Content>
      </Navbar>

      <div id="imgcontainer">
        <Image id="bg" src="/assets/other/bg.png" objectFit="cover" showSkeleton alt=""/>
          <div id="imgtext">
            <motion.div
              initial={{ scale: 0 }}
              animate={{ scale: 1 }}
              transition={{ type: "spring", stiffness: 100 }}
            >
              <Text h1 className='centered' css={{textGradient: "45deg, $blue600 -20%, $purple700 50%"}}>Hello, I am Wyndix.</Text>
              <Text h2 css={{textGradient: "45deg, $gray900 -20%, $gray900 50%"}}>
                Retarded at day, developer at night
              </Text>
            </motion.div>
          </div>
      </div>

      <div id="reviewcontainer">
        <Grid.Container gap={2} justify="center">
          <Grid>
            <UserCard name="LenEnjoyer" pfp="/assets/profiles/LenEnjoyer.webp" info="Gamer" text="pls leave us alone" pronouns="he/him" />
          </Grid>
          <Grid>
            <UserCard name="Kevadesu" pfp="/assets/profiles/Kevadesu.gif" info="Coder" text="[Wyndix] PLEASE STOP YOU'RE SCARING ME" pronouns="they/them" />
          </Grid>
          <Grid>
            <UserCard name="Mizu" pfp="/assets/profiles/Mizu.webp" info="Hatsune" text="FUCK OFF" pronouns="they/them" />
          </Grid>
          <Grid>
            <UserCard name="Laniku" pfp="/assets/profiles/Laniku.png" info="Coder" text="Sometimes Wyndix horrifies me" pronouns="they/them" />
          </Grid>
          <Grid>
            <UserCard name="[REDACTED]" pfp="/assets/profiles/YoungGroupie.jpeg" info="Music Producer" text="He's so emo, but mostly he's respectful 🥰" pronouns="they/them" />
          </Grid>
          <Grid>
            <UserCard name="Prisixia" pfp="/assets/profiles/Prisixia.png" info="Coder" text="[Wyndix] is definitely an emo." pronouns="they/them" />
          </Grid>
          <Grid>
            <UserCard name="StanDX" pfp="/assets/profiles/StanDX.jpg" info="Music Maker" text="[Wyndix] is absolutely crazy, he's delusional and doesn't know when to quit." pronouns="they/them" />
          </Grid>
          <Grid>
            <UserCard name="yuavibez" pfp="/assets/profiles/yuavibez.webp" info="Music Maker" text="I think [Wyndix] is the deffinition of propoganda and manipulation" pronouns="they/them" />
          </Grid>
        </Grid.Container>
      </div>

      <Grid.Container gap={0} justify="center">
        <div class="maincontainer">
          <Text h1>
                Reach me out
          </Text>

          <Grid.Container gap={2} justify="center">
            <Grid>
              <Collapse.Group shadow>
                <Collapse
                  title={<Link h4 href="https://fosstodon.org/web/@Wyndix">Mastodon</Link>}
                  contentLeft={
                    <Avatar
                      size="lg"
                      src="/assets/logos/masto.png"
                      color="primary"
                      bordered
                      squared
                    />
                  }
                >
                  <Text>
                    Here I post random stuff and shitpost a lot, feel free to follow me so that we can interact!
                  </Text>
                </Collapse>
                <Collapse
                  title={<Link h4='https://codeberg.org/wyndix'>Codeberg</Link>}
                  contentLeft={
                    <Avatar
                      size="lg"
                      src="/assets/logos/codeberg.png"
                      color="success"
                      bordered
                      squared
                    />
                  }
                >
                  <Text>
                    Here is where I have most of my projects, some unfinished. May make an Org soon!
                  </Text>
                </Collapse>
                <Collapse
                  title={<Link h4 href="https://matrix.to/#/@wyndix:envs.net">Matrix</Link>}
                  contentLeft={
                    <Avatar
                      size="lg"
                      src="/assets/logos/element.svg"
                      color="error"
                      bordered
                      squared
                    />
                  }
                >
                  <Text>
                    Main chat platform I use, see you in the Matrix ;)
                  </Text>
                </Collapse>
              </Collapse.Group>
            </Grid>
          </Grid.Container>
        </div>
      
        <div class="maincontainer">
          <Text h1>
            Complaints and business stuff
          </Text>

          <Grid.Container gap={2}>
            <Grid>
              <Collapse.Group shadow>
                <Collapse
                  title={<Link h4 href="mailto: wyndix@tuta.io">Email me</Link>}
                  contentLeft={
                    <Avatar
                      size="lg"
                      src="/assets/logos/Message.svg"
                      color="none"
                      style={{backgroundColor: 'white'}}
                      bordered
                      squared
                    />
                  }
                >
                  <Text>
                    My main email, feel free to use it for any sort of thing in the case you don't happen to have any of my social media.
                  </Text>
                </Collapse>
              </Collapse.Group>
            </Grid>
          </Grid.Container>
        </div>
      </Grid.Container>
    </NextUIProvider>
  );
}

function UserCard(props) {
  return (
    <motion.div
      initial={{ scale: 0.5, y: 200 }}
      whileInView={{ scale: 1, y: 1 }}
      viewport={{ once: true }}
      transition={{
        type: "spring",
        stiffness: 45,
        damping: 10
      }}
    >
      <Card css={{ p: "$6", mw: "400px", margin: "10px" }} className='card' isHoverable variant="bordered">
        <Card.Header>
            <User
              src={props.pfp}
              name={props.name}
              description={props.info}
            />
        </Card.Header>
        <Card.Body css={{ py: "$2" }}>
          <Text i class="reviewtext">
            "{props.text}"
          </Text>
        </Card.Body>
      </Card>
    </motion.div>
  )
}

export default App;